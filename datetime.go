package utils

import (
    "time"
)

// 获取delta天后的时间，输出string
func GetDate(delta int) string{
    t := time.Now()
    t = t.AddDate(0,0,delta)
    return t.Format("20060102")
}