package utils

import "encoding/json"

type MapT map[string]interface{}

type MapI interface {
	get(k string) interface{}
	getOrDefault(k string, default_value interface{})
}

func (obj MapT) Get(k string) interface{} {
	return obj[k]
}

func (obj MapT) GetOrDefault(k string, default_value interface{}) interface{} {
	res := obj[k]
	if res == nil {
		return default_value
	} else {
		return res
	}
}

func ConvertToMapT(obj interface{}) MapT {

	switch obj.(type) {
	case map[string]interface{}:
		m := obj.(map[string]interface{})
		return (MapT)(m)
	case MapT:
		return obj.(MapT)
	default:
		return (MapT)(make(map[string]interface{}))

	}
}

// bytes 转 interface{}
func UnMarshal(b []byte) (obj interface{}) {

	err := json.Unmarshal(b, &obj)
	if err != nil {
		panic(err)
	}
	return obj
}
